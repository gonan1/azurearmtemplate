---
description: This template allows you to create a Virtual Machines with multiple (2) network interfaces (NICs), and RDP connectable with a configured load balancer and an inbound NAT rule. More NICs can easily  be added with this template. This template also deploys a Storage Account, Virtual Network, Public IP address, and 2 Network Interfaces (front-end and back-end).
page_type: sample
products:
- azure
- azure-resource-manager
urlFragment: 1-vm-loadbalancer-2-nics
languages:
- json
---
# Create a VM with multiple NICs and RDP accessible

[![Deploy To Azure](https://raw.githubusercontent.com/Azure/azure-quickstart-templates/master/1-CONTRIBUTION-GUIDE/images/deploytoazure.svg?sanitize=true)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fraw.githubusercontent.com%2FAzure%2Fazure-quickstart-templates%2Fmaster%2Fquickstarts%2Fmicrosoft.compute%2F1-vm-loadbalancer-2-nics%2Fazuredeploy.json)

[![Deploy To Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.com%2Fgonan1%2Fazurearmtemplate%2F-%2Fraw%2Fmain%2Fazuredeploy.json)


This template allows you to create a Virtual Machine with multiple (2) network interfaces (NICs), and RDP connectable with a configured load balancer and an inbound NAT rule. More NICs can easily be added with this template. This template also deploys a Storage Account, Virtual Network, Public IP address, and 2 Network Interfaces (front-end and back-end).

`Tags: Microsoft.Storage/storageAccounts, Microsoft.Network/loadBalancers, Microsoft.Network/networkSecurityGroups, Microsoft.Network/networkInterfaces, Microsoft.Network/publicIPAddresses, Microsoft.Compute/virtualMachines, Microsoft.Network/virtualNetworks`
